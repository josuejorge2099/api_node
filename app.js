var bodyparser = require('body-parser');
var express = require('express');
var cors = require('cors');
var app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded( { extended: true }));
app.use(cors());

//endpoints
app.post('/api/login', function(req, res){
    var email = req.body.email;
    var senha = req.body.senha;

    if( email != 'jooj@gmail.com' || senha != '123456'){
        setTimeout(function(){
               res.send(401,{
                    'erro':{
                        'http_code':401,
                        'code': 'unautorized',
                        'mensagem': 'Login e/ou senha inválidos!'
                    }
               }) 
        },4000);
    }else{
        setTimeout(function(){
              res
                .header('Acess-Control-Allow-Origin', '*')
                .send(200,{
                    'data':{
                        'nome':'jooj',
                        'email': 'jooj@gmail.com',
                        'token': 'token_aqui123@qwe'
                    }   
                })  
        },4000);
    }
});

app.listen(3000);
console.log('servidor rodando');